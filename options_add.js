(function($) {
  Drupal.behaviors.optionsAdd = {
    attach: function() {
      var opt_add = {};
      opt_add.Dependent = {};
      opt_add.Dependent.comparisons = {
        'Array': function(reference, value) {
          //Make sure that value is an array, other wise we end up always evaling to true
          if(!( typeof (value) == 'object' && ( value instanceof Array))) {
            return false;
          }
          //We iterate through each of the values provided in the reference
          //and check that they all exist in the value array.
          //If even one doesn't then we return false. Otherwise return true.
          var arrayComplies = true;
          $.each(reference, function(key, val) {
            if($.inArray(val, value) < 0) {
              arrayComplies = false;
            }
          });
          return arrayComplies;
        },
      };
      var states = Drupal.states;
      $.extend(true, states, opt_add);
      Drupal.states = states;
    }
  }
})(jQuery);

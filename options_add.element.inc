<?php
/**
 * @file
 *   Element processing for the options_add element type
 */
/**
 * Generates the Options Add form element.
 */
function _options_add_element_process(&$element, &$form_state, $form) {
  static $counter;
  if (!$counter) {
    $counter = 0;
  }
  $counter++;
  $type = $element['#options_type'];
  $multiple = isset($element['#multiple']) ? $element['#multiple'] : NULL;
  $option_id = drupal_html_id('option_add_another');
  $element['wrap'] = array(
    '#type' => 'container',
    '#id' => $option_id,
  );
  $element['wrap']['options'] = array(
    '#title' => $element['#title'],
    '#type' => $type,
    '#default_value' => $element['#default_value'],
    '#attributes' => array('class' => array('options_add-select-'.$counter)),
    // Do not display a 'multiple' select box if there is only one option.
    '#multiple' => $multiple,
    '#options' => $element['#options'],
  );
  switch ($type) {
    case 'select':
      $state_value = $multiple ? array('_add'): '_add';
      if ($multiple) {
        drupal_add_js(drupal_get_path('module', 'options_add') . '/options_add.js');
      }
      
      $field_id = str_replace(']', '', $element['#name']);
      $field_id = str_replace('[', '-', $field_id);
      $field_id = str_replace('_', '-', $field_id);
      $field_id = 'edit-' . $field_id . '-options-add-another';
      
      $state = 'select.options_add-select-'.$counter ;
      $element['wrap']['options_add_another'] = array(
        '#prefix' => '<div id="'.$field_id.'" class="form-item form-wrapper">',
        '#suffix' => '</div>',
      );
      $element['wrap']['options_add_another']['#states'] = array(
        'visible' => array(
          $state => array('value' => $state_value),
        ),
      );
      $element['wrap']['options_add_another']['options_add'] = array(
        '#type' => 'textfield',
        '#title' => t('Add Another'),
      );
      $element['wrap']['options_add_another']['options_add_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Add'),
        '#submit' => array('options_add_another_submit'),
        '#name' => $option_id . '-btn',
        '#aajax' => array(
          'wrapper' => $option_id,
          'callback' => 'options_add_ajax',
          'method' => 'replace',
          'effect' => 'fade',
        ),        
      );
      break;
    case 'radio':
    case 'checkboxes':
      break;
  }

  unset($element['#options']);
  return $element;
}

/**
 * Validates the Options Add form element.
 */
function _options_add_element_validate(&$element, &$form_state) {
  _options_add_prep_element($element, $form_state);
  $value = $element['#value'];
  form_set_value($element, $value, $form_state);
}